import { Component, OnInit } from '@angular/core';
import { projects } from './projects';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  projects : Array<projects> = [
     {serialNumber : 1,Name : "Note Taker Application",techUsed : "adv.java,hibernate,mysql",duration:2}
    ,{serialNumber : 2,Name : "Luxury Car Company",techUsed : "Tailwind Css",duration:1}
    ,{serialNumber : 3,Name : "Library Management System",techUsed : "java GUI,jdbc,mysql",duration:2}
    ,{serialNumber : 4,Name : "POC Banking Application",techUsed : "Angular Js, ASP.NET, MySQL",duration:6}
    ,{serialNumber : 5,Name : "portfolio website",techUsed : "angular Js",duration:1}
  ]

}
