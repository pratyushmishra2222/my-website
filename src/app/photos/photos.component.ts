import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {

  constructor() { }

  images_url : string[] = ["../../assets/pictures/pic2.jpg",
  "../../assets/pictures/pic3.jpg","../../assets/pictures/pic4.jpg","../../assets/pictures/pic5.jpg","../../assets/pictures/pic6.jpg"];
  
  
  data : any = [
    {
        url : "../../assets/pictures/pic2.jpg",
        date : "12-2-2019"
    },
    {
        url : "../../assets/pictures/pic3.jpg",
        date : "12-2-2018"
    },
    {
        url : "../../assets/pictures/pic4.jpg",
        date : "12-3-2017"
    },
    {
      url : "../../assets/pictures/pic5.jpg",
      date : "12-3-2017"
    },
    {
      url : "../../assets/pictures/pic6.jpg",
      date : "25-12-2018"
    }
    
  ]
  
  ngOnInit(): void {
  }

}
