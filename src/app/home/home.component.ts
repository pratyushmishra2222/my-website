import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private httpclient : HttpClient,private title : Title) { }

  images_url : string[] = ["../../assets/pictures/ds-algo.jpeg",
  "../../assets/pictures/internet.png",
  "../../assets/pictures/web-dev.jpeg"];
  
  data : any = [
    {
        url : "../../assets/pictures/photography.jpg",
        content : "Mobile photography is the art of photography using a smartphone or mobile device..",
        value:"mobile photography"
    },
    {
        url : "../../assets/pictures/football.jpg",
        content : "football is a team sport played with a spherical ball between two teams of 11 players.",
        value:"football"
    },
    {
        url : "../../assets/pictures/web-dev.jpeg",
        content : " Web dev is the work involved in developing a Web site for the intranet or the internet.",
        value:" Web development"
    }
]


  ngOnInit(): void {this.title.setTitle('home')}
}
