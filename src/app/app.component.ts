import { transition, trigger, useAnimation } from '@angular/animations';
import { Component } from '@angular/core';
import { moveFromLeft } from 'ngx-router-animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:  [
    trigger('moveFromLeft',  [ transition('* => *', useAnimation(moveFromLeft))])
    ]
})
export class AppComponent {
  title = 'portfolio';
  getState(outlet: any)  {
		return outlet.activatedRouteData.state;
	}
}
